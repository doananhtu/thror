module ApplicationHelper
	#To solve the problem of a missing page title
	def full_title(page_title = '')
		base_title = "Ruby on Rails"
		page_title.empty? ? base_title : "#{page_title} | #{base_title}"
	end
end
